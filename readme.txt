=== Suma Patches ===

Contributors: David Sinclair, Corey Mitchell

Requires at least: 5.2.2

Tested up to: 5.2.2

Requires PHP: 7.2

Contains small snippets of code to modify the PHP environment to the Rhino Group Standards

== Description ==

Contains small snippets of code to modify the PHP environment to the Rhino Group Standards

== Installation ==

Upload the zip and hit activate.

== Constants ==
SUMA_PATCHES_DISABLE_PLUGINS_ENABLE - Disables the plugins that are not needed on dev/staging sites. (true/false)
SUMA_PATCHES_ADMIN_EMAIL - Overrides all emails to send to this email address. (Email Address)
SUMA_PATCHES_ENABLE_DEV_CHECK - Enables the dev check to see if the site is in dev mode. (true/false)
SUMA_PATCHES_ENABLE_UPDATE_CHECK - Enables the dev check for updates. (true/false)
SUMA_PATCHES_MAILGUN_FROM_EMAIL - Overrides the from email for mailgun. (Email Address)
SUMA_PATCHES_MAILGUN_FROM_NAME - Overrides the from name for mailgun. (Name)
SUMA_PATCHES_MAILGUN_REPLY_TO - Overrides the reply to for mailgun. (Email Address)
SUMA_PATCHES_SEND_ALL_EMAILS_TO - Overrides all emails to send to this email address. (Email Address)

== Changelog ==
1.5.19
- Enhancements - Added in more constants to be defined. (SUMA_PATCHES_ENABLE_UPDATE_CHECK, SUMA_PATCHES_MAILGUN_FROM_EMAIL, SUMA_PATCHES_MAILGUN_FROM_NAME, SUMA_PATCHES_MAILGUN_REPLY_TO, SUMA_PATCHES_SEND_ALL_EMAILS_TO)
1.5.18
- Enhancement - Allow things to be configured via Constants (SUMA_PATCHES_DISABLE_PLUGINS_ENABLE, SUMA_PATCHES_ADMIN_EMAIL, SUMA_PATCHES_ENABLE_DEV_CHECK)
1.5.17
- Tweak - Added Walmart Plugin to list to deactivate.
1.5.16
- Fix - Update to switch url for new bazaarvoice integrator to the stg upload on dev conversion.
1.5.15
- Fix - Algolia index name transformation now does not put double __ in the index name.
1.5.14
- Feature - Disable BigCommerce Product Sync on Dev Conversion
1.5.13
- Fix - Removed Authentication Line for Gitlab plugin after making repo public.
1.5.12.2
- Tweak - Remove Deprecated Algolia Hook
1.5.12.1
- Fix - Revert removing WP Mail SMTP
1.5.12
- Tweak - Added in Review Reminder Plugin to Dev Disable List
1.5.11
 - Feature - Changed WP Rocket Bar to be red and added - disabled if the disabled wp rocket suma settings is turned on.
1.5.10
 - Feature - Auto Configure WP Mail SMTP for Herd when on local environment with .test extension.
1.5.9
 - Feature - Secures the WordPress admin login error page with error message that doesn't provide information.
 - Dev - Update PUC to 5.4
1.5.8
 - Feature - Add Toggle to Disable Users api from being accessible without authentication.
1.5.7
 - Feature - Add referrer to Algolia API calls to prevent indexing from staging and dev sites.
1.5.6
 - Tweak - Added New Algolia Search Plugin into Plugins Dev List
1.5.5
 - Fix - Email rerouting on dev was not working because it only ran once instead of hooking all the time.

1.5.4
 - Feature - Made it so Woocommerce adds a note to all orders when an email is sent out to the customer.

1.5.3
 - Feature - Added in an API to detect read-only file system on WP Engine. This will integrate directly with the MR and show us which sites are not up-to-date

1.5.2
 - Fix - Image Hooks: Correct missing class exception on upload of media.

1.5.1
 - Fix - Auto-updater works again after a bug breaking it in 1.5.0.

1.5.0
 - Feature - Added WP Rocket cache clear feature for development.
 - Refactored some code to give a slight performance boost
 - Added a setting to turn on and off the WP Rocket cache clearing feature.
 - Updated a setting for Dev mode to a dropdown.

1.4.1
 - Fix - Small BugFix to email fields.

1.4.0
 - Fix - Image uploads broken due to php8 incompatibility.

1.3.0
- Tweak - Added old algolia plugin to list of disabled plugins.

1.2.0
- Feature - Disabled a lot of plugins that are not needed on staging/dev sites to prevent sites from indexing improperly.
- Feature - Override all emails from the system to send to webteam@rhinogroup.com.

1.1.0
- Feature - Added SMTP Override From Email and Name Fields
- Fix - Broken Reply To Override on WooCommerce Emails.

1.0.8
- Feature - Added Mailgun Global Reply To Field
- Fix - Broken external requests when using extension.

1.0.7
- Added option to
