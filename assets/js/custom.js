var SumaPatchesModel = function () {
	var self        = this;
	self.baseurl    = '/wp-admin/admin-post.php';
	self.num        = 100;
	self.count      = 1;
	self.total      = 1;
	self.totalPages = 1;

	/**
	 *  Global get ajax method
	 *  @param {Object} args - parameters to pass to get.
	 *  @param {string} callback - Function that gets called on completion
	 *  @param {string} errorCallback - Function that gets called in case of an error
	 *  @param {string} successCallback - Function that gets called in case of success
	 */
	self.get = function (args, callback, errorCallback, successCallback) {
		jQuery.ajax({
			method: "GET",
			url: self.baseurl,
			data: args,
			cache: false
		}).done(function (data) {
			if (data && data.status == "error") {
				if (errorCallback) {
					errorCallback(data.message);
				}
			} else {
				if (successCallback) {
					successCallback();
				}
			}
			if (callback) {
				callback(data);
			}
		});
	};

	/**
	 *  Global post ajax method
	 *  @param {Object} args - parameters to pass to get.
	 *  @param {string} callback - Function that gets called on completion
	 *  @param {string} errorCallback - Function that gets called in case of an error
	 *  @param {string} successCallback - Function that gets called in case of success
	 */
	self.post = function (args, callback, errorCallback, successCallback) {
		jQuery.ajax({
			method: "POST",
			url: self.baseurl,
			data: args,
			cache: false,
			async: true
		}).done(function (data) {
			if (data && data.status === "error") {
				if (errorCallback) {
					errorCallback(data.message);
				}
			} else {
				if (successCallback) {
					successCallback();
				}
			}
			if (callback) {
				var res = callback(data);
				if (res !== undefined) {
					return res;
				}
			}
		});
	};
	self.getNumAttachments  = function () {
		self.get({'action': 'suma_get_num_attachments'}, function (results) {
			self.total      = results;
			self.totalPages = Math.ceil(self.total / self.num);
			jQuery('.suma-total-image-count').text(self.total);
			jQuery('.suma-total-images').show();
			self.processAttachments(self.count);
		});
	};
	self.processAttachments = function (page) {
		self.get({'action': 'suma_update_alt_images', 'page': page}, function (results) {
			var data = JSON.parse(results);
			if (data.status === "ok") {
				if (self.count < self.totalPages) {
					jQuery('.suma-percentage').text(Math.ceil((self.count / self.totalPages) * 100));
					self.count++;
					self.processAttachments(self.count);
				} else {
					jQuery('.suma-percentage').text(100);
					jQuery('.suma-working').hide();
					jQuery('.suma-done').css('display', 'inline-block');
					console.log('done');
				}
			}
		});
	};

	self.init = function () {
		jQuery('.update-alts').on('click', function (e) {
			e.preventDefault();
			jQuery('.suma-working').show();
			self.getNumAttachments();
		});
	};
};
var SumaPatchesApp   = '';
jQuery(function () {
	SumaPatchesApp = new SumaPatchesModel();
	SumaPatchesApp.init();
});
