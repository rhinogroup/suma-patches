<?php
require_once( dirname( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) ) . '/wp-load.php' );

class Suma_Patches_API {
	/**
	 * API constructor.
	 */
	public function __construct() {
		$action  = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : '';
		switch ( $action ) {
			case 'clear_autoptimize_cache':
				$this->clear_autoptimize_cache();
				break;

			case 'get_options':
				$this->get_options();
				break;
		}
	}

	public function clear_autoptimize_cache() {
		if ( class_exists( 'autoptimizeCache' ) ) {
			autoptimizeCache::clearall();
		}
	}

	public function get_options() {
		$status       = [
			'result'  => 'ok',
			'message' => ''
		];
		$options_name = isset( $_REQUEST['options_name'] ) ? $_REQUEST['options_name'] : '';
		$api_password = isset( $_REQUEST['api_password'] ) ? $_REQUEST['api_password'] : '';
		$unique_id    = get_option( 'mainwp_child_uniqueId' );
		$allowed_ips  = [
			'12.235.66.14',
			'12.235.66.22',
			'72.10.49.235'
		];
		if ( $api_password === $unique_id && in_array( $_SERVER['REMOTE_ADDR'], $allowed_ips ) ) {
			$options     = explode( '|', $options_name );
			$all_options = [];
			foreach ( $options as $option ) {
				$all_options[ $option ] = get_option( $option );
			}
			$status['message'] = $all_options;
		} else {
			$status['result']  = 'error';
			$status['message'] = 'You are not authorized to access this site.';
		}
		echo json_encode( $status );
	}
}
$Suma_API = new Suma_Patches_API();
