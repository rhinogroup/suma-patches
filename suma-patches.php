<?php
/*
Plugin Name: Suma Patches
Plugin URI: https://www.rhinogroup.com
Description: WordPress Specific patches to enable/disable features in Core to meet Rhino Group Standards.
Author: Rhino Group
Author URI: https://www.rhinogroup.com
Text Domain: suma-patches
Version: 1.5.19
*/

namespace Suma;

class Patches {

	public function init() {
		define( 'SUMAPATCHESROOT', plugin_dir_path( __FILE__ ) );
		require_once( plugin_dir_path( __FILE__ ) . 'inc/class-system.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'inc/class-admin.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'inc/class-frontend.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'inc/class-images.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'inc/class-woocommerce.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'inc/class-algolia.php' );

		new Patches\System();

		new Patches\Admin();

		new Patches\Frontend();

		new Patches\Images();

		new Patches\WooCommerce();

		new Patches\Algolia();
	}
}

( new Patches() )->init();
