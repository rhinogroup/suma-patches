<?php

namespace Suma\Patches;

class Frontend {
	
	public function __construct() {
		
		add_action( 'wp_head', array( $this, 'render_head' ) );
		add_action( 'wp_footer', array( $this, 'render_footer' ) );
		add_action( 'wp', array( $this, 'clear_wprocket_cache' ), 999 );
	}
	
	public function clear_wprocket_cache() {
		
		if ( is_admin() || is_robots() || is_favicon() || wp_doing_ajax() )
			return;
		
		if ( 'yes' == get_option( 'suma_wprocket_cache_disabled' ) ) {
			if ( function_exists( 'rocket_clean_minify' ) ) {
				rocket_clean_minify();
			}
			
			if ( function_exists( 'rocket_clean_domain' ) ) {
				rocket_clean_domain();
			}
		}
	}

	public function render_head() {
		$head    = get_option( 'suma_head_scripts' );
		$ga_code = get_option( 'suma_ga_code' );
		if ( ! empty( $ga_code ) ) {
			?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $ga_code; ?>"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', '<?php echo $ga_code;?>');
            </script>
			<?php
		}
		if ( ! empty( $head ) ) {
			echo $head;
		}
	}

	public function render_footer() {
		$footer = get_option( 'suma_footer_scripts' );
		if ( ! empty( $footer ) ) {
			echo $footer;
		}
	}
}