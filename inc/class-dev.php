<?php

namespace Suma\Patches;
require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

class Dev {
	public $non_dev_plugins = [
		'woocommerce-abandoned-cart/woocommerce-ac.php',
		'woocommerce-abandon-cart-pro/woocommerce-ac.php',
		'woo-order-export-lite/woo-order-export-lite.php',
		'autoptimize/autoptimize.php',
		'back-in-stock-notifier-for-woocommerce/cwginstocknotifier.php',
		'bwp-minify/bwp-minify.php',
		'cloudflare/cloudflare.php',
		'facebook-for-woocommerce/facebook-for-woocommerce.php',
		'official-facebook-pixel/facebook-for-wordpress.php',
		'woocommerce-follow-up-emails/woocommerce-follow-up-emails.php',
		'getresponse-integration/gr-loader.php',
		'enhanced-e-commerce-for-woocommerce-store/enhanced-ecommerce-google-analytics.php',
		'google-analytics-dashboard-for-wp/gadwp.php',
		'google-analytics-for-wordpress/googleanalytics.php',
		'duracelltomi-google-tag-manager/duracelltomi-google-tag-manager-for-wordpress.php',
		'gravityformsaweber/aweber.php',
		'gravity-forms-constant-contact/constantcontact.php',
		'gravityformsconstantcontact/constantcontact.php',
		'gravityformsgetresponse/getresponse.php',
		'gravityformsmailchimp/mailchimp.php',
		'klaviyo/klaviyo.php',
		'mailchimp-for-woocommerce/mailchimp-woocommerce.php',
		'mailgun/mailgun.php',
		'monsterinsights-dimensions/monsterinsights-dimensions.php',
		'ga-ecommerce/ga-ecommerce.php',
		'monsterinsights-forms/monsterinsights-forms.php',
		'monsterinsights-google-optimize/monsterinsights-google-optimize.php',
		'monsterinsights-performance/monsterinsights-performance.php',
		'google-analytics-premium/googleanalytics-premium.php',
		'onesignal-free-web-push-notifications/onesignal.php',
		'postman-smtp/postman-smtp.php',
		'really-simple-ssl/rlrsssl-really-simple-ssl.php',
		'review-reminder-for-woocommerce/review-reminder-for-woocommerce.php',
		'search-by-algolia-instant-relevant-results/algolia.php',
		'tenpoint-integrator/tenpoint-integrator.php',
		'tracking-code-manager/index.php',
		'tracking-code-manager-pro/index.php',
		'waitlist-woocommerce/xoo-wl-main.php',
		'walmart-integration-for-woocommerce/walmart-woocommerce-integration.php',
		'woocommerce-subscriptions/woocommerce-subscriptions.php',
		'woocommerce-shipstation-integration/woocommerce-shipstation.php',
		'woocommerce-avatax/woocommerce-avatax.php',
		'woocommerce-constant-contact/woocommerce-constant-contact.php',
		'woocommerce-google-analytics-integration/woocommerce-google-analytics-integration.php',
		'woocommerce-product-feeds/woocommerce-gpf.php',
		'woocommerce-mailchimp-integration/woocommerce-mailchimp-integration.php',
		'woocommerce-google-adwords-conversion-tracking-tag/wgact.php',
		'myworks-woo-sync-for-quickbooks-online/myworks-woo-sync-for-quickbooks-online.php',
		'woocommerce-zapier/woocommerce-zapier.php',
		'wp-mail-smtp/wp_mail_smtp.php',
		//'wp-rocket/wp-rocket.php',
		'yith-woocommerce-wishlist-premium/init.php',
		'wp-search-with-algolia/algolia.php'
	];
	public $plugins_to_deactivate;

	public function init() {
		add_action( 'init', array( $this, 'deactivate_plugins' ) );
		add_action( 'init', array( $this, 'adjust_algolia_index_to_dev' ) );
		add_action( 'init', array( $this, 'update_admin_email_address' ) );
		add_action( 'init', array( $this, 'noindex_website' ) );
		add_action( 'init', array( $this, 'update_bazaarvoice_integrator' ) );
		add_action( 'init', array( $this, 'add_robots_file' ) );
		add_action( 'init', array( $this, 'avatax_disable_sending_orders' ) );
		add_action( 'init', array( $this, 'disable_bigcommerce_sync' ) );
		add_action( 'init', array( $this, 'remove_suma_ga_code' ) );
		if(system::is_herd()) {
			add_action( 'init', array( $this, 'fix_wp_mail_smtp' ) );
		}

	}

	public function deactivate_plugins() {
		$deactivate = true;
		if(defined('SUMA_PATCHES_DISABLE_PLUGINS_ENABLE')) {
			$deactivate = SUMA_PATCHES_DISABLE_PLUGINS_ENABLE;
		}
		if($deactivate){
			$plugins = get_option( 'active_plugins' );
			foreach ( $plugins as $plugin ) {
				if ( in_array( $plugin, $this->non_dev_plugins ) ) {
					if(system::is_herd() && $plugin == 'wp-mail-smtp/wp_mail_smtp.php') {
						continue;
					}
					$this->plugins_to_deactivate[] = $plugin;
				}
				deactivate_plugins( $this->plugins_to_deactivate, true );
			}
		}

	}


	public function adjust_algolia_index_to_dev() {
		$option = get_option( 'algolia_index_name_prefix' );
		if ( strpos( $option, '_dev_' ) === false ) {
			update_option( 'algolia_index_name_prefix', str_replace('__','_',$option . '_dev_') );
		}
	}


	public function update_admin_email_address() {
		$email = 'webteam@rhinogroup.com';
		if(defined('SUMA_PATCHES_ADMIN_EMAIL')) {
			$email = SUMA_PATCHES_ADMIN_EMAIL;
		}
		update_option( 'admin_email', $email );
	}

	public function noindex_website() {
		update_option( 'blog_public', '0' );
	}

	public function update_bazaarvoice_integrator() {
		$config = get_option( 'bazaarvoice_option_name' );
		if ( is_array( $config ) ) {
			$config['bv_sftp_host'] = 'sftp-stg.bazaarvoice.com';
			update_option( 'bazaarvoice_option_name', $config );
		}
		update_option('suma-bazaarvoice-integrator_sftp_host','sftp-stg.bazaarvoice.com');
	}

	public function add_robots_file() {
		file_put_contents( ABSPATH . 'robots.txt', 'User-agent: *' . PHP_EOL . 'Disallow: /' );
	}

	public function avatax_disable_sending_orders(){
		update_option( 'wc_avatax_record_calculations', 'no' );
		update_option( 'wc_avatax_commit', 'no' );
	}

	public function remove_suma_ga_code(){
		update_option( 'suma_ga_code', '' );
	}

	public function fix_wp_mail_smtp(){
		$smtp = get_option( 'wp_mail_smtp' );
		if ( is_array( $smtp ) ) {
			$url = explode('.',parse_url(get_site_url(), PHP_URL_HOST ));
			$url = $url[0];
			$smtp['mail']['mailer'] = 'smtp';
			$smtp['smtp'] = [
				'autotls' => 'yes',
				'auth' => 'yes',
				'host' => '127.0.0.1',
				'port' => 2525,
				'encryption' => 'none',
				'user' => $url,
				'pass' => '123456',
			];
			update_option( 'wp_mail_smtp', $smtp );
		}
	}

	public function disable_bigcommerce_sync(){
		update_option( 'bigcommerce_import_frequency', 'never' );
	}
}
