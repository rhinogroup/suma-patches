<?php
namespace Suma\Patches;

class Algolia{
	public function __construct(){
		add_filter('algolia_http_client_options', [$this,'update_algolia_referrer'],10,1);
	}
	public function update_algolia_referrer($options){
		$urlparts = wp_parse_url(home_url());
		$domain = $urlparts['host'];
		$options['CURLOPT_REFERER'] = $domain;
		return $options;
	}
}
