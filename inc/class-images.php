<?php

namespace Suma\Patches;

class Images {
	
	public function __construct() {
		
		add_action( 'add_attachment', [ $this, 'update_alt_on_image' ] );
		add_action( 'admin_post_suma_update_alt_images', [ $this, 'update_alt_tags_on_images' ] );
		add_action( 'admin_post_suma_get_num_attachments', [ $this, 'get_num_attachments' ] );
	}

	public static function update_alt_on_image( $post_ID ) {
		// Check if uploaded file is an image, else do nothing
		if ( wp_attachment_is_image( $post_ID ) ) {
			$cur     = get_post( $post_ID );
			$cur_alt = get_post_meta( $post_ID, '_wp_attachment_image_alt', true );
			if ( empty( $cur_alt ) || empty( $cur->post_title ) ) {
				$image_post_parent = $cur->post_parent;
				if ( $image_post_parent > 0 ) {
					$image_title = get_post( $image_post_parent )->post_title;
				} else {
					$image_title = $cur->post_title;
				}

				// Sanitize the title:  remove hyphens, underscores & extra spaces:
				$image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ', $image_title );

				// Sanitize the title:  capitalize first letter of every word (other letters lower case):
				$image_title = ucwords( strtolower( $image_title ) );

				// Create an array with the image meta (Title, Caption, Description) to be updated
				// Note:  comment out the Excerpt/Caption or Content/Description lines if not needed
				$image_meta = array(
					'ID'         => $post_ID,            // Specify the image (ID) to be updated
					'post_title' => $image_title,        // Set image Title to sanitized title
				);
				if ( empty( $cur_alt ) ) {
					// Set the image Alt-Text
					update_post_meta( $post_ID, '_wp_attachment_image_alt', $image_title );
				}
				if ( empty( $cur->post_title ) ) {
					// Set the image meta (e.g. Title, Excerpt, Content)
					wp_update_post( $image_meta );
				}
			}
		}
	}

	public static function update_alt_tags_on_images() {
		$page   = isset( $_REQUEST['page'] ) ? $_REQUEST['page'] : 1;
		$offset = ( $page - 1 ) * 100;
		$args   = [
			'post_type'      => 'attachment',
			'post_status'    => 'inherit',
			'posts_per_page' => 100,
			'page'           => $page,
			'fields'         => 'ids',
			'offset'         => $offset

		];
		$query  = new \WP_Query( $args );
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				self::update_alt_on_image( get_the_ID() );
			}
		}
		echo json_encode( array( 'status' => 'ok', 'message' => '' ) );
		die();
	}

	public static function get_num_attachments() {
		global $wpdb;
		$count = $wpdb->get_var( "SELECT COUNT(id) FROM {$wpdb->prefix}posts WHERE post_parent != 0 AND post_type = 'attachment'" );
		echo $count;
	}
}
