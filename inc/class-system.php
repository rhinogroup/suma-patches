<?php

namespace Suma\Patches;

class System {

	public function __construct() {

		add_filter( 'recovery_mode_email', array( $this, 'recovery_mode_email' ) );
		add_filter( 'wp_mail', array( $this, 'add_mailgun_reply_to' ), 999 );
		add_filter( 'wp_mail_from', array( $this, 'change_email_from' ), 999 );
		add_filter( 'wp_mail_from_name', array( $this, 'change_email_from_name' ), 999 );
		add_filter( 'wp_mail', array( $this, 'send_all_emails_to_same_email' ), 99999, 1 );
		//add_filter( 'autoptimize_filter_speedupper', '__return_false' );

		add_action('wp_loaded', array( $this, 'wp_rocket_disabled' ) );

		$this->dev_site_check();
	}

	public static function is_heartbeat() {

		return isset( $_REQUEST['action'] ) && $_REQUEST['action'] === 'heartbeat';
	}

	public static function is_favicon() {

		return isset( $_SERVER['REQUEST_URI'] ) && $_SERVER['REQUEST_URI'] === '/favicon.ico';
	}

	public function dev_site_check() {

		if ( self::is_heartbeat() || self::is_favicon() ) {
			return;
		}

		if ( defined( 'DEV_CHECK_DONE' ) ) {
			return;
		}
		$run_dev_check = true;
		if ( defined( 'SUMA_PATCHES_ENABLE_DEV_CHECK' ) ) {
			$run_dev_check = SUMA_PATCHES_ENABLE_DEV_CHECK;
		}
		if($run_dev_check){
			require_once( plugin_dir_path( __FILE__ ) . '../inc/class-dev.php' );

			if ( System::is_dev() ) {

				$option = get_option( 'suma_is_dev_site' );

				if ( $option !== 'yes' ) {
					$dev = new Dev();
					$dev->init();

					update_option( 'suma_is_dev_site', 'yes' );
				}

				define( 'DEV_CHECK_DONE', 1 );
			}
		}

	}

	/**
	 * Modify the recovery mode email address.
	 */
	public function recovery_mode_email( $email_data ) {
		$email_data['to'] = 'webteam@rhinogroup.com';

		return $email_data;
	}

	public function change_email_from( $original_email_address ) {
		$email = get_option( 'suma_patches_mailgun_from_email' );
		if ( defined( 'SUMA_PATCHES_MAILGUN_FROM_EMAIL' ) ) {
			$email = SUMA_PATCHES_MAILGUN_FROM_EMAIL;
		}

		if ( empty( $email ) || ! is_email( $email ) ) {
			return $original_email_address;
		}

		return $email;
	}

	public function change_email_from_name( $original_name ) {
		$name = get_option( 'suma_patches_mailgun_from_name' );
		if ( defined( 'SUMA_PATCHES_MAILGUN_FROM_NAME' ) ) {
			$name = SUMA_PATCHES_MAILGUN_FROM_NAME;
		}
		if ( empty( $name ) ) {
			return $original_name;
		}

		return $name;
	}

	public function add_mailgun_reply_to( $args ) {
		$email = get_option( 'suma_patches_mailgun_reply_to' );
		if ( defined( 'SUMA_PATCHES_MAILGUN_REPLY_TO' ) ) {
			$email = SUMA_PATCHES_MAILGUN_REPLY_TO;
		}
		if ( is_email( $email ) ) {
			foreach ( $args['headers'] as $key => $header ) {
				if ( strpos( strtolower( $header ), 'reply-to:' ) ) {
					unset( $args['header'][ $key ] );
				}
			}
			if ( is_array( $args['headers'] ) ) {
				$args['headers'][] = 'Reply-To: ' . $email;
			} else {
				if ( ! empty( $args['headers'] ) ) {
					$args['headers']   = [ $args['headers'] ];
					$args['headers'][] = 'Reply-To: ' . $email;
				}
			}

		}

		return $args;
	}

	public function send_all_emails_to_same_email( $args ) {
		if ( System::is_dev() ) {
			$option = get_option( 'suma_is_dev_site' );
			if ( $option === 'yes' ) {
				$email = 'webteam@rhinogroup.com';
				if ( defined( 'SUMA_PATCHES_SEND_ALL_EMAILS_TO' ) ) {
					$email = SUMA_PATCHES_SEND_ALL_EMAILS_TO;
				}
				$args['to']      = $email;
				$args['subject'] = $args['subject'] . ' ON DEV SERVER: ' . get_site_url();
				$args['headers'] = "Content-Type: text/html";
				$address = get_option( 'suma_patches_dev_to_address_override' );
				if ( ! empty( $address ) && is_email( $address ) ) {
					$args['to'] = $address;
				}
			}
		}

		return $args;
	}

	public static function is_dev() {
		$domains_to_look_for = [
			"dev.wpengine",
			"dev2.wpengine",
			"dev3.wpengine",
			"stg.wpengine",
			"stg2.wpengine",
			"stg3.wpengine",
			"beta.",
			"staging.",
			".cc",
			".test"
		];
		$url                 = get_site_url();

		if ( str_replace( $domains_to_look_for, '', $url ) != $url ) {
			return true;
		}

		return false;
	}

	public static function is_herd() {
		$domains_to_look_for = [
			".test"
		];
		$url = get_site_url();

		if ( str_replace( $domains_to_look_for, '', $url ) != $url ) {
			return true;
		}

		return false;
	}

	public function wp_rocket_disabled(){
		if ( is_robots() || is_favicon() || wp_doing_ajax() )
			return;
		$enabled =  get_option( 'suma_wprocket_cache_disabled' );
		if($enabled === 'yes'){
			add_action( 'wp_head', array( $this, 'wp_rocket_disabled_css' ) );
			add_action( 'admin_head', array( $this, 'wp_rocket_disabled_css' ) );
		}

	}
	public function wp_rocket_disabled_css(){
		?>
        <style>
            #wp-admin-bar-wp-rocket{
                background:red!important;
            }
            #wp-admin-bar-wp-rocket .ab-item:after {
                content: " - Disabled";
            }
        </style>
		<?php
	}
}
