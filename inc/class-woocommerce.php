<?php

namespace Suma\Patches;

class WooCommerce {
	public function __construct() {
		add_action( 'plugins_loaded', [ $this, 'init' ] );
	}

	public function init() {
		if ( class_exists( 'WooCommerce' ) ) {
			add_action( 'woocommerce_email_sent', [ $this, 'add_note_on_email_sent' ], 10, 3 );
		}
	}

	public function add_note_on_email_sent( $success, $email_id, $email ) {
		if ($email->object instanceof \WC_Order) {
			$order = $email->object;
			$order->add_order_note(
				sprintf(
					__( '%s email sent to customer.', 'woocommerce' ),
					$email->get_title()
				)
			);
		}
	}
}