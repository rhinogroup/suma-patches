<?php

namespace Suma\Patches;
class Admin {

	/**
	 * Constructor for the Admin class.
	 */
	public function __construct() {

		add_action( 'admin_menu', array( $this, 'register_settings_page' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );
        add_action( 'init', [ $this, 'register_update_checker' ] );

		add_action( 'wp_ajax_sumatra_wordpress_read_only_filesystem', array( $this, 'read_only_filesystem' ) );
		add_action( 'wp_ajax_nopriv_sumatra_wordpress_read_only_filesystem', array( $this, 'read_only_filesystem' ) );
		add_action( 'rest_authentication_errors', [ $this, 'disable_unauthenticated_user_rest_api' ] );

		add_filter( 'login_errors', [$this,'secure_login_error'] );
	}

	/**
     * Registers the update checker for the plugin.
	 * @return void
	 */
	public function register_update_checker() {
        $enable = true;
        if(defined('SUMA_PATCHES_ENABLE_UPDATE_CHECK')){
            $enable = SUMA_PATCHES_ENABLE_UPDATE_CHECK;
        }
        if($enable) {
	        require SUMAPATCHESROOT . 'lib/plugin-update-checker/plugin-update-checker.php';

	        $update_checker = \YahnisElsts\PluginUpdateChecker\v5\PucFactory::buildUpdateChecker(
		        'https://gitlab.com/rhinogroup/suma-patches/',
		        SUMAPATCHESROOT . 'suma-patches.php',
		        'suma-patches'
	        );

	        //Optional: Set the branch that contains the stable release.
	        $update_checker->setBranch( 'release' );
        }
	}

	/**
     * Registers the settings page for the plugin.
	 * @return void
	 */
	public function register_settings_page() {
		add_submenu_page( 'options-general.php', 'Suma Settings', 'Suma Settings', 'manage_options', 'suma-patches', array( $this, 'render_options' ) );
	}

	/**
     * Registers the settings for the plugin.
	 * @return void
	 */
	public function register_settings() {
		//register our settings
		register_setting( 'suma_patches_options', 'suma_ga_code' );
		register_setting( 'suma_patches_options', 'suma_head_scripts' );
		register_setting( 'suma_patches_options', 'suma_footer_scripts' );
		register_setting( 'suma_patches_options', 'suma_patches_mailgun_reply_to' );
		register_setting( 'suma_patches_options', 'suma_patches_mailgun_from_email' );
		register_setting( 'suma_patches_options', 'suma_patches_mailgun_from_name' );
		register_setting( 'suma_patches_options', 'suma_patches_dev_to_address_override' );
		register_setting( 'suma_patches_options', 'suma_is_dev_site' );
		register_setting( 'suma_patches_options', 'suma_wprocket_cache_disabled' );
		register_setting( 'suma_patches_options', 'suma_wp_rest_users_api_must_authenticate' );
	}

	/**
     * Renders the options page.
	 * @return void
	 */
	public function render_options() {
		?>
		<style>
			.suma-working{
				margin-left:10px;
			}
			.suma-spinner{
				margin-left:8px;
			}
			.suma-done{
				background: #008000;
				padding: 5px 15px;
				color: #FFF;
				margin-left: 15px;
			}
		</style>
        <script src="/wp-content/plugins/suma-patches/assets/js/custom.js"></script>
        <div class="wrap">
            <h1>Suma Settings</h1>

            <form method="post" action="options.php">
				<?php settings_fields( 'suma_patches_options' ); ?>
				<?php do_settings_sections( 'suma_patches_options' ); ?>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">GA Code</th>
                        <td><input type="text" name="suma_ga_code" value="<?php echo esc_attr( get_option('suma_ga_code') ); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Head Scripts</th>
                        <td><textarea rows="8" cols="80" type="text" id="suma_head_scripts" name="suma_head_scripts" ><?php echo esc_attr( get_option('suma_head_scripts') ); ?></textarea></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Footer Scripts</th>
                        <td><textarea rows="8" cols="80" type="text"  id="suma_footer_scripts" name="suma_footer_scripts" ><?php echo esc_attr( get_option('suma_footer_scripts') ); ?></textarea></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">SMTP From Email</th>
                        <td><input type="text" name="suma_patches_mailgun_from_email" value="<?php echo esc_attr( get_option('suma_patches_mailgun_from_email') ); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">SMTP From Name</th>
                        <td><input type="text" name="suma_patches_mailgun_from_name" value="<?php echo esc_attr( get_option('suma_patches_mailgun_from_name') ); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">SMTP Reply To</th>
                        <td><input type="text" name="suma_patches_mailgun_reply_to" value="<?php echo esc_attr( get_option('suma_patches_mailgun_reply_to') ); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><h2>DEV SETTINGS</h2></th>
                        <td>These settings only apply to sites determined to be development by suma patches.</td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">To Address Override:</th>
                        <td><input type="text" name="suma_patches_dev_to_address_override" value="<?php echo esc_attr( get_option('suma_patches_dev_to_address_override') ); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Site is Dev:</th>
                        <td><select name="suma_wprocket_cache_disabled">
                                <option value="no">No</option>
                                <option value="yes" <?php echo get_option('suma_is_dev_site') == 'yes' ? ' selected ' : ''; ?>>Yes</option>
                            </select>
                            <span>Set to no to redo staging site conversion process.</span>
                            </td>
                    </tr>
	                <tr valign="top">
		                <th scope="row">Disable WPRocket Cache:</th>
		                <td>
			                <select name="suma_wprocket_cache_disabled">
				                <option value="no">No</option>
				                <option value="yes" <?php echo get_option('suma_wprocket_cache_disabled') == 'yes' ? ' selected ' : ''; ?>>Yes</option>
			                </select>
			                <span>When set to Yes, WPRocket caching will be disabled.</span>
		                </td>
	                </tr>
                    <tr valign="top">
                        <th scope="row">Users Enpdoint Requires Auth:</th>
                        <td>
                            <select name="suma_wp_rest_users_api_must_authenticate">
                                <option value="yes">Yes</option>
                                <option value="no" <?php echo get_option('suma_wp_rest_users_api_must_authenticate') == 'no' ? ' selected ' : ''; ?>>No</option>
                            </select>
                            <span>When set to Yes, Users Rest API endpoint requires the user to be logged in to access it.</span>
                        </td>
                    </tr>
                </table>
				<?php submit_button(); ?>
            </form>
            <h1>Other Tools</h1>
            <div class="container">
                <div class="row">
                    <div class="suma-total-images hidden">Total Images Optimizing: <span class="suma-total-image-count"></span></div>
                    <a href="#" class="update-alts button button-primary">Assign Post Title to Empty Alt Tags on Images</a><span class="suma-working hidden"><span class="suma-percentage">0</span>%<img src="/wp-includes/js/tinymce/skins/lightgray/img/loader.gif" width="20" height="20" class="suma-spinner"/></span><span class="suma-done hidden">Complete</span>
                </div>
            </div>
        </div>
		<?php
	}

	/**
     * Checks if the filesystem is read only.
	 * @return void
	 */
	public function read_only_filesystem(){
	    header("Content-type: application/json; charset=utf-8");
	    $ro = defined('WPE_RO_FILESYSTEM') && WPE_RO_FILESYSTEM;
	    echo json_encode([ 'readonly' => $ro]);
        die();
    }

	/**
     * //Protects the Users endpoint from unauthenticated users.
	 * @param $access
	 *
	 * @return mixed|\WP_Error
	 */
	public function disable_unauthenticated_user_rest_api($access){

        if ( is_user_logged_in() ) {
            return $access;
        }
	    $option = get_option( 'suma_wp_rest_users_api_must_authenticate' );
	    if ( $option !== 'no' ) {
		    if ( ( preg_match( '/users/i', $_SERVER['REQUEST_URI'] ) !== 0 )
		         || ( isset( $_REQUEST['rest_route'] ) && ( preg_match( '/users/i', $_REQUEST['rest_route'] ) !== 0 ) )
		    ) {
			    return new \WP_Error(
				    'rest_cannot_access',
				    'Only authenticated users can access the User endpoint REST API.',
				    [
					    'status' => rest_authorization_required_code()
				    ]
			    );
		    }
	    }
        return $access;
    }

	/**
     * Overrides the default WordPress Login Error to make it secure.
	 * @return string
	 */
	public function secure_login_error(){
		return 'You have entered an invalid username or password. Please try again!';
	}

}
